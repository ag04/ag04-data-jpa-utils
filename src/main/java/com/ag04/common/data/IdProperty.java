package com.ag04.common.data;

/**
 *
 * @author Luka Marasovic, AG04 on 14.12.2015.
 */
public interface IdProperty<T> {
    T getId();
}
