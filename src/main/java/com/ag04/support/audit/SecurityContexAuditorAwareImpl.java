package com.ag04.support.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 * Created by dmadunic on 29/03/16.
 */
public class SecurityContexAuditorAwareImpl implements AuditorAware<String> {

    /**
     *
     * @return username property of org.springframework.security.core.userdetails.User
     */
    @Override
    public String getCurrentAuditor() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return user.getUsername();
    }
}
