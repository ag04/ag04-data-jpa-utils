package com.ag04.common.data.converter;

import com.ag04.common.data.IdProperty;

import javax.persistence.AttributeConverter;

/**
 *
 * @author Luka Marasovic, AG04 on 14.12.2015.
 */
public class EnumTypeConverter <T extends Enum & IdProperty<Tid>, Tid> implements AttributeConverter<T, Tid> {

    private Class<T> enumClazz;

    public EnumTypeConverter(Class<T> enumClazz) {
        this.enumClazz = enumClazz;
    }

    @Override
    public Tid convertToDatabaseColumn(T attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.getId();
    }

    @Override
    public T convertToEntityAttribute(Tid dbData) {
        for (T enumValue : enumClazz.getEnumConstants()) {
            if (enumValue.getId().equals(dbData)) {
                return enumValue;
            }
        }
        return null;
    }
}
