package com.ag04.support.web;

import com.ag04.common.data.PageableUtil;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by dmadunic on 19/04/16.
 */
public class RequestParamUtils {

    public static Pageable pageable(final Integer page, final Integer size, String sortAttributes, Map<String, String> keyPropertyMap) {
        PageableUtil.PageableBuilder builder = PageableUtil.getBuilder(keyPropertyMap);
        return buildPageable(page, size, sortAttributes, builder);
    }

    public static Pageable pageable(final PageableParams pageableParams) {
        PageableUtil.PageableBuilder builder = PageableUtil.getBuilder();
        return buildPageable(pageableParams, builder);
    }

    public static Pageable pageable(final Integer page, final Integer size, String sortAttributes) {
        PageableUtil.PageableBuilder builder = PageableUtil.getBuilder();
        return buildPageable(page, size, sortAttributes, builder);
    }

    private static Pageable buildPageable(final PageableParams pageableParams, PageableUtil.PageableBuilder builder) {
        if (pageableParams.getPage() != null) {
            builder.setPage(pageableParams.getPage());
        }
        if (pageableParams.getSize() != null) {
            builder.setSize(pageableParams.getSize());
        }
        if (pageableParams.getSortableAttributes() != null && !pageableParams.getSortableAttributes().isEmpty()) {
            builder.addSortAttributes(pageableParams.getSortableAttributes());
        }
        return builder.build();
    }

    private static Pageable buildPageable(Integer page, Integer size, String sortAttributes, PageableUtil.PageableBuilder builder) {
        if (page != null) {
            builder.setPage(page);
        }
        if (size != null) {
            builder.setSize(size);
        }
        if (sortAttributes != null) {
            List<String> sortAttributeList = parseCommaDelimitedValues(sortAttributes);
            builder.addSortAttributes(sortAttributeList);
        }
        return builder.build();
    }

    private static List<String> parseCommaDelimitedValues(String inputString) {
        List<String> valueList = new ArrayList<>();
        if (inputString != null) {
            String[] paramArray = inputString.split(",");
            valueList.addAll(Arrays.asList(paramArray));
        }
        return valueList;
    }
}
