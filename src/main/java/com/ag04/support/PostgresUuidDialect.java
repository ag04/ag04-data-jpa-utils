package com.ag04.support;

import org.hibernate.boot.model.TypeContributions;
import org.hibernate.dialect.PostgreSQL9Dialect;

import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.PostgresUUIDType;

/**
 *
 * @author Luka Marasovic, AG04 on 21.12.2015.
 */
public class PostgresUuidDialect extends PostgreSQL9Dialect {

    @Override
    public void contributeTypes(final TypeContributions typeContributions, final ServiceRegistry serviceRegistry) {
        super.contributeTypes(typeContributions, serviceRegistry);
        typeContributions.contributeType(new InternalPostgresUUIDType());
    }

    protected static class InternalPostgresUUIDType extends PostgresUUIDType {

        @Override
        protected boolean registerUnderJavaType() {
            return true;
        }
    }
}
