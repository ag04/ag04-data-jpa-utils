package com.ag04.support.audit;

import org.springframework.data.domain.AuditorAware;

/**
 * Created by dmadunic on 29/03/16.
 */
public class MockAuditorAwareStringImpl implements AuditorAware<String> {

    private String defaultUserId;

    @Override
    public String getCurrentAuditor() {
        return defaultUserId;
    }

    //--- set / get methods ---------------------------------------------------

    public void setDefaultUserId(String defaultUserId) {
        this.defaultUserId = defaultUserId;
    }

}
