ag04-data-jpa-utils
====================

Ag04 various Java JPA data (mainly test) utils.

## Usage
### Requirements
* [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Maven](https://maven.apache.org/download.cgi)

### Setup (First time)
1. Clone the repository: `git clone https://YoutUserName@bitbucket.org/ag04/ag04-data-jpa-utils.git`
4. Build project with: ` mvn clean install `

### Release

1) mvn release:prepare
2) mvn release:perform

## Changelog

### Version 0.0.6

Upgraded:

* spring-boot to version 1.4.5.RELEASE 
* dbunit to 2.5.3 

### Version 0.0.5

Upgraded:

* spring-boot to version 1.3.6.RELEASE 
* spring-test-dbunit to 1.3.0, 
* dbunit to 2.5.2 
* javaassist 3.20.0-GA

also cleaned up unnecessary lib/jar version definitions so that now all dependencies are resolved through spring.

#### Dependencies:

* spring-boot :: 1.3.6.RELEASE
    * spring-boot-starter
    * spring-boot-starter-data-jpa
    * spring-boot-starter-test
    * spring-jdbc
    * spring-test
    * spring-security-core

* joda-time :: 2.8.2 (resolved through spring-boot-parent)

* junit 4.12 (resolved through spring-boot-parent)

* dbunit 2.5.2

* mockito-core :: 1.10.19 (resolved through spring-boot-parent)

* javaassist 3.20.0-GA

* fest-assert 1.4

* spring-test-dbunit 1.3.0
