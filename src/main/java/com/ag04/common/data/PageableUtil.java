package com.ag04.common.data;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Luka Marasovic, AG04 on 8.12.2015..
 */
public class PageableUtil {

    private static final int DEFAULT_PAGE_SIZE = 25;

    private static final int DEFAULT_PAGE = 0;

    private static final char DESCEND_PREFIX = '-';

    /**
     * not intended for instantiation
     */
    private PageableUtil() {
    }

    public static PageableBuilder getBuilder(Map<String, String> keyProperyMap) {
        return new DictionaryPageableBuilder(DEFAULT_PAGE, DEFAULT_PAGE_SIZE, keyProperyMap);
    }

    public static PageableBuilder getBuilder() {
        return new PageableBuilder(DEFAULT_PAGE, DEFAULT_PAGE_SIZE);
    }

    private static Pageable constructPageable(PageableBuilder builder) {
        if (builder.sortAttributes.isEmpty()) {
            return new PageRequest(builder.page, builder.size);

        } else {
            return new PageRequest(builder.page, builder.size, constructSort(builder.sortAttributes));
        }
    }

    private static Sort constructSort(List<String> sortableAttributes) {
        Sort sort = null;
        for (String attribute : sortableAttributes) {
            Sort.Direction direction = Sort.Direction.ASC;
            String propertyField = attribute;
            if (attribute.charAt(0) == DESCEND_PREFIX) {
                direction = Sort.Direction.DESC;
                propertyField = attribute.substring(1);
            }
            Sort currentSort = new Sort(direction, propertyField);
            sort = sort == null ? currentSort : sort.and(currentSort);
        }

        return sort;
    }

    public static class PageableBuilder {

        protected List<String> sortAttributes = new ArrayList<>();
        private int size;
        private int page;

        private PageableBuilder(int page, int size) {
            this.page = page;
            this.size = size;
        }

        public PageableBuilder setSize(Integer size) {
            if (size != null && size >= 1) {
                this.size = size;
            }
            return this;
        }

        public PageableBuilder setPage(Integer page) {
            if (page != null && page >= 0) {
                this.page = page;
            }
            return this;
        }

        public PageableBuilder addSortAttributes(List<String> sortAttributeList) {
            if (sortAttributeList != null) {
                sortAttributes.addAll(sortAttributeList);
            }
            return this;
        }

        public Pageable build() {
            return PageableUtil.constructPageable(this);
        }
    }

    public static class DictionaryPageableBuilder extends PageableBuilder {

        private Map<String, String> keyPropertyMap;

        private DictionaryPageableBuilder(int page, int size, Map<String, String> keyPropertyMap) {
            super(page, size);
            this.keyPropertyMap = keyPropertyMap;
        }

        @Override
        public DictionaryPageableBuilder addSortAttributes(List<String> sortAttributeList) {
            if (sortAttributeList != null) {
                for (String attribute : sortAttributeList) {
                    String propertyPath = getPropertyPath(attribute);
                    if (propertyPath != null) {
                        sortAttributes.add(propertyPath);
                    }
                }
            }
            return this;
        }

        private String getPropertyPath(String attribute) {
            if (attribute.charAt(0) == DESCEND_PREFIX) {
                String propertyPath = keyPropertyMap.get(attribute.substring(1));
                return propertyPath != null ? DESCEND_PREFIX + propertyPath : null;
            } else {
                return keyPropertyMap.get(attribute);
            }
        }
    }
}
