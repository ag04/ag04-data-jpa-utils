package com.ag04.support.web;

import java.util.List;

/**
 * Created by lovrostrihic on 24.06.16..
 */
public interface PageableParams {

    Integer getPage();

    Integer getSize();

    List<String> getSortableAttributes();
}
